<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('admin', 'IndexController@index')->name('login'); // for redirection purpose


/*Route::name('/')->group(
    function () {*/

        Route::get('/', 'IndexController@index');
        Route::group(['middleware' => 'backendAuthenticate'], function () {

            Route::get('/dashboard', [
                'uses' => 'DashboardController@index',
                'as' => 'dashboard.index'
            ]);

            Route::group(['middleware' => ['backendAuthenticate','PermissionHandler']], function () {


                ###role management routes###
                Route::resource('role', 'RoleController');
                Route::get('role/set-permissions/{role}', 'RoleController@setPermissions')->name('role.set-permissions');
                Route::post('role/set-permissions/update/{role}', 'RoleController@setPermissionsUpdate')->name('role.set-permissions.update');

                ###Sub Admins###
                Route::get('sub-admin/data', 'SubAdminController@data')->name('sub-admin.data');
                Route::resource('sub-admin', 'SubAdminController');
                Route::get('sub-admin/active/{record}', 'SubAdminController@active')->name('sub-admin.active');
                Route::get('sub-admin/inactive/{record}', 'SubAdminController@inactive')->name('sub-admin.inactive');


                Route::get('zones/data', 'ZonesController@data')->name('zones.data');
                Route::resource('zones', 'ZonesController');


                Route::get('work-time/data', 'WorkTimeController@data')->name('work-time.data');
                Route::resource('work-time', 'WorkTimeController');


                Route::get('work-type/data', 'WorkTypeController@data')->name('work-type.data');
                Route::resource('work-type', 'WorkTypeController');

                Route::get('joey-document-verification/data', 'JoeyDocumentVerificationController@data')->name('joey-document-verification.data');
                Route::resource('joey-document-verification', 'JoeyDocumentVerificationController');
                Route::get('joeyDocumentVerification/status/update/statusUpdate', 'JoeyDocumentVerificationController@statusUpdate')->name('joey-document-verification.statusUpdate');


                Route::get('training/data', 'TrainingController@data')->name('training.data');
                Route::resource('training', 'TrainingController');

                Route::get('categores/data', 'CategoresController@data')->name('categores.data');
                Route::resource('categores', 'CategoresController');


                Route::get('quiz-management/data', 'QuizController@data')->name('quiz-management.data');
                Route::resource('quiz-management', 'QuizController');


                Route::get('job-type/data', 'JobTypeController@data')->name('job-type.data');
                Route::resource('job-type', 'JobTypeController');


                Route::get('order-category/data', 'OrderCategoryController@data')->name('order-category.data');
                Route::resource('order-category', 'OrderCategoryController');

                Route::get('vendors/data', 'VendorsController@data')->name('vendors.data');
                Route::resource('vendors', 'VendorsController');

                Route::get('joey-checklist/data', 'JoeyChecklistController@data')->name('joey-checklist.data');
                Route::resource('joey-checklist', 'JoeyChecklistController');


                Route::get('basic-vendor/data', 'BasicVendorController@data')->name('basic-vendor.data');
                Route::resource('basic-vendor', 'BasicVendorController');


                Route::get('basic-category/data', 'BasicCategoryController@data')->name('basic-category.data');
                Route::resource('basic-category', 'BasicCategoryController');

                Route::get('vendor-score/data', 'VendorScoreController@data')->name('vendor-score.data');
                Route::resource('vendor-score', 'VendorScoreController');

                Route::get('category-score/data', 'CategoryScoreController@data')->name('category-score.data');
                Route::resource('category-score', 'CategoryScoreController');


                Route::resource('site-settings', 'SiteSettingsController');
                Route::resource('countries', 'CountriesController');
                Route::resource('cities', 'CitiesController');


                Route::get('/edit-profile', [
                    'uses' => 'UsersController@editProfile',
                    'as' => 'users.edit-profile'
                ]);

                Route::post('/edit-profile', [
                    'uses' => 'UsersController@updateEditProfile',
                    'as' => 'users.edit-profile'
                ]);


                Route::get('/change-password', [
                    'uses' => 'UsersController@changePassword',
                    'as' => 'users.change-password'
                ]);


                Route::post('/change-password', [
                    'uses' => 'UsersController@processChangePassword',
                    'as' => 'users.change-password'
                ]);
            });
        }
        );
          Route::get('/login', [
                'uses' => 'Auth\LoginController@showLoginForm',
                'as' => 'login'
            ]);

        Route::post('/login', [
            'uses' => 'Auth\LoginController@login',
            'as' => 'login'
        ]);


        Route::any('/logout', [
            'uses' => 'Auth\LoginController@logout',
            'as' => 'logout'
        ]);

        Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
        Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
        Route::get('reset-password', 'auth\ResetPasswordController@reset_password_from_show')->name('reset.password.show');
        Route::post('reset-password-update', 'auth\Resetpasswordcontroller@reset_password_update')->name('reset.password.update');
  //}
//);

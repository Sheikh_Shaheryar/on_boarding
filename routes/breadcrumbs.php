<?php

/**
 * routes/breadcrumbs.php
 *
 * @author Muzafar Ali Jatoi <muzfr7@gmail.com>
 * @Date: 19/9/18
 */

/*
|--------------------------------------------------------------------------
| Admin Dashboard
|--------------------------------------------------------------------------
*/

// Dashboard
Breadcrumbs::for('dashboard.index', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard.index'));
});



/*
|--------------------------------------------------------------------------
| Role & Permissions
|--------------------------------------------------------------------------
*/


Breadcrumbs::for('role.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Role List', route('role.index'));
});

// Pages > New
Breadcrumbs::for('role.create', function ($breadcrumbs) {
    $breadcrumbs->parent('role.index');
    $breadcrumbs->push('Add', route('role.create'));
});

// Pages > Show
Breadcrumbs::for('role.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('role.index');
    $breadcrumbs->push('Show', route('role.show', $data->id));
});


// Pages > Edit
Breadcrumbs::for('role.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('role.index', $data);
    $breadcrumbs->push('Edit', route('role.edit', $data->id));
});

// Pages > Set Permissions
Breadcrumbs::for('role.set-permissions', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('role.index', $data);
    $breadcrumbs->push('Set Permissions', route('role.set-permissions', $data->id));
});


/*
|--------------------------------------------------------------------------
| Sub Admin
|--------------------------------------------------------------------------
*/


Breadcrumbs::for('sub-admin.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Sub Admin List', route('sub-admin.index'));
});

// Pages > New
Breadcrumbs::for('sub-admin.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sub-admin.index');
    $breadcrumbs->push('Add', route('sub-admin.create'));
});

// Pages > Show
Breadcrumbs::for('sub-admin.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('sub-admin.index');
    $breadcrumbs->push('Show', route('sub-admin.show', $data->id));
});

// Pages > Edit
Breadcrumbs::for('sub-admin.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('sub-admin.index', $data);
    $breadcrumbs->push('Edit', route('sub-admin.edit', $data->id));
});



/*
|--------------------------------------------------------------------------
| Zones
|--------------------------------------------------------------------------
*/

// Zones  > Listing
Breadcrumbs::for('zones.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Zone List', route('zones.index'));
});

// Zones > New
Breadcrumbs::for('zones.create', function ($breadcrumbs) {
    $breadcrumbs->parent('zones.index');
    $breadcrumbs->push('Add', route('zones.create'));
});

// Zones > Show
Breadcrumbs::for('zones.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('zones.index');
    $breadcrumbs->push('Show', route('zones.show', $data->id));
});

// zones > Edit
Breadcrumbs::for('zones.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('zones.index', $data);
    $breadcrumbs->push('Edit', route('zones.edit', $data->id));
});




/*
|--------------------------------------------------------------------------
| Work Time
|--------------------------------------------------------------------------
*/

// Work Time  > Listing
Breadcrumbs::for('work-time.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Work Time List', route('work-time.index'));
});

// workTime > New
Breadcrumbs::for('work-time.create', function ($breadcrumbs) {
    $breadcrumbs->parent('work-time.index');
    $breadcrumbs->push('Add', route('work-time.create'));
});

// workTime > Show
Breadcrumbs::for('work-time.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('work-time.index');
    $breadcrumbs->push('Show', route('work-time.show', $data->id));
});

// workTime > Edit
Breadcrumbs::for('work-time.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('work-time.index', $data);
    $breadcrumbs->push('Edit', route('work-time.edit', $data->id));
});


/*
|--------------------------------------------------------------------------
| Work Type
|--------------------------------------------------------------------------
*/

// workType  > Listing
Breadcrumbs::for('work-type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Work Type List', route('work-type.index'));
});

// workType > New
Breadcrumbs::for('work-type.create', function ($breadcrumbs) {
    $breadcrumbs->parent('work-type.index');
    $breadcrumbs->push('Add', route('work-type.create'));
});

// workType > Show
Breadcrumbs::for('work-type.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('work-type.index');
    $breadcrumbs->push('Show', route('work-type.show', $data->id));
});

// workType > Edit
Breadcrumbs::for('work-type.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('work-type.index',$data);
    $breadcrumbs->push('Edit', route('work-type.edit', $data->id));
});






/*
|--------------------------------------------------------------------------
| Joey Document verification
|--------------------------------------------------------------------------
*/

// Joey Document verification> Listing
Breadcrumbs::for('joey-document-verification.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Joey Document Verification List', route('joey-document-verification.index'));
});

Breadcrumbs::for('joey-document-verification.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('joey-document-verification.index');
    $breadcrumbs->push('Show', route('joey-document-verification.show', $data->id));
});


/*
|--------------------------------------------------------------------------
|Training
|--------------------------------------------------------------------------
*/

// Training  > Listing
Breadcrumbs::for('training.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Training List', route('training.index'));
});

// Training > New
Breadcrumbs::for('training.create', function ($breadcrumbs) {
    $breadcrumbs->parent('training.index');
    $breadcrumbs->push('Add', route('training.create'));
});



/*
|--------------------------------------------------------------------------
| Categores
|--------------------------------------------------------------------------
*/

// Categores  > Listing
Breadcrumbs::for('categores.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Categores List', route('categores.index'));
});

// Categores > New
Breadcrumbs::for('categores.create', function ($breadcrumbs) {
    $breadcrumbs->parent('categores.index');
    $breadcrumbs->push('Add', route('categores.create'));
});

// Categores > Show
Breadcrumbs::for('categores.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('categores.index');
    $breadcrumbs->push('Show', route('categores.show', $data->id));
});

// Categores > Edit
Breadcrumbs::for('categores.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('categores.index',$data);
    $breadcrumbs->push('Edit', route('categores.edit', $data->id));
});





/*
|--------------------------------------------------------------------------
| Quiz Management
|--------------------------------------------------------------------------
*/

// quizManagement  > Listing
Breadcrumbs::for('quiz-management.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('quiz Management List', route('quiz-management.index'));
});

// quizManagement > New
Breadcrumbs::for('quiz-management.create', function ($breadcrumbs) {
    $breadcrumbs->parent('quiz-management.index');
    $breadcrumbs->push('Add', route('quiz-management.create'));
});

// quizManagement > Show
Breadcrumbs::for('quiz-management.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('quiz-management.index');
    $breadcrumbs->push('Show', route('quiz-management.show', $data->id));
});

// quizManagement > Edit
Breadcrumbs::for('quiz-management.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('quiz-management.index',$data);
    $breadcrumbs->push('Edit', route('quiz-management.edit', $data));
});

/*
|--------------------------------------------------------------------------
| Vendors
|--------------------------------------------------------------------------
*/

// Vendors  > Listing
Breadcrumbs::for('vendors.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Vendors Order Count List', route('vendors.index'));
});

// Vendors > New
Breadcrumbs::for('vendors.create', function ($breadcrumbs) {
    $breadcrumbs->parent('vendors.index');
    $breadcrumbs->push('Add', route('vendors.create'));
});

// Vendors > Show
Breadcrumbs::for('vendors.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('vendors.index');
    $breadcrumbs->push('Show', route('vendors.show', $data->id));
});

// Vendors > Edit
Breadcrumbs::for('vendors.edit', function ($breadcrumbs, $data) {

    $breadcrumbs->parent('vendors.index',$data);

    $breadcrumbs->push('Edit', route('vendors.edit', $data->id));
});



/*
|--------------------------------------------------------------------------
| Joey Checklist
|--------------------------------------------------------------------------
*/


Breadcrumbs::for('joey-checklist.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Joeyes CheckList', route('joey-checklist.index'));
});

// Joey Checklist > New
Breadcrumbs::for('joey-checklist.create', function ($breadcrumbs) {
    $breadcrumbs->parent('joey-checklist.index');
    $breadcrumbs->push('Add', route('joey-checklist.create'));
});

// Joey Checklist> Show
Breadcrumbs::for('joey-checklist.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('joey-checklist.index');
    $breadcrumbs->push('Show', route('joey-checklist.show', $data->id));
});

// Joey Checklist> Edit
Breadcrumbs::for('joey-checklist.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('joey-checklist.index', $data);
    $breadcrumbs->push('Edit', route('joey-checklist.edit', $data->id));
});



/*
|--------------------------------------------------------------------------
| basic vendor
|--------------------------------------------------------------------------
*/


Breadcrumbs::for('basic-vendor.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Basic Vendor List', route('basic-vendor.index'));
});

// basic vendor> New
Breadcrumbs::for('basic-vendor.create', function ($breadcrumbs) {
    $breadcrumbs->parent('basic-vendor.index');
    $breadcrumbs->push('Add', route('basic-vendor.create'));
});



/*
|--------------------------------------------------------------------------
| basic category
|--------------------------------------------------------------------------
*/


Breadcrumbs::for('basic-category.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Basic Category List', route('basic-category.index'));

});

// basic vendor> New
Breadcrumbs::for('basic-category.create', function ($breadcrumbs) {
    $breadcrumbs->parent('basic-category.index');
    $breadcrumbs->push('Add', route('basic-category.create'));
});


/*
|--------------------------------------------------------------------------
| Vendor Score
|--------------------------------------------------------------------------
*/


Breadcrumbs::for('vendor-score.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Vendor Quizes Score List', route('vendor-score.index'));
});

//  Vendor Score> New
Breadcrumbs::for('vendor-score.create', function ($breadcrumbs) {
    $breadcrumbs->parent('vendor-score.index');
    $breadcrumbs->push('Add', route('vendor-score.create'));
});

//  Vendor Score> Show
Breadcrumbs::for('vendor-score.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('vendor-score.index');
    $breadcrumbs->push('Show', route('vendor-score.show', $data->id));
});

//  Vendor Score> Edit
Breadcrumbs::for('vendor-score.edit', function ($breadcrumbs, $data) {

    $breadcrumbs->parent('vendor-score.index', $data);
    $breadcrumbs->push('Edit', route('vendor-score.edit', $data->id));
});

/*
|--------------------------------------------------------------------------
| Category Score
|--------------------------------------------------------------------------
*/


Breadcrumbs::for('category-score.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Categories Quizes Score List', route('category-score.index'));
});

//  Vendor Score> New
Breadcrumbs::for('category-score.create', function ($breadcrumbs) {
    $breadcrumbs->parent('category-score.index');
    $breadcrumbs->push('Add', route('category-score.create'));
});

//  Vendor Score> Show
Breadcrumbs::for('category-score.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('category-score.index');
    $breadcrumbs->push('Show', route('category-score.show', $data->id));
});

//  Vendor Score> Edit
Breadcrumbs::for('category-score.edit', function ($breadcrumbs, $data) {

    $breadcrumbs->parent('category-score.index', $data);
    $breadcrumbs->push('Edit', route('category-score.edit', $data->id));
});



/*
|--------------------------------------------------------------------------
| Job Type
|--------------------------------------------------------------------------
*/


Breadcrumbs::for('job-type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Job Type List', route('job-type.index'));
});

// Pages > New
Breadcrumbs::for('job-type.create', function ($breadcrumbs) {
    $breadcrumbs->parent('job-type.index');
    $breadcrumbs->push('Add', route('job-type.create'));
});

// Pages > Show
Breadcrumbs::for('job-type.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('job-type.index');
    $breadcrumbs->push('Show', route('job-type.show', $data->id));
});

// Pages > Edit
Breadcrumbs::for('job-type.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('job-type.index', $data);
    $breadcrumbs->push('Edit', route('job-type.edit', $data->id));
});


/*
|--------------------------------------------------------------------------
| Order Category
|--------------------------------------------------------------------------
*/


Breadcrumbs::for('order-category.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Order Categories List', route('order-category.index'));
});

// Pages > New
Breadcrumbs::for('order-category.create', function ($breadcrumbs) {
    $breadcrumbs->parent('order-category.index');
    $breadcrumbs->push('Add', route('order-category.create'));
});

// Pages > Show
Breadcrumbs::for('order-category.show', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('order-category.index');
    $breadcrumbs->push('Show', route('order-category.show', $data->id));
});

// Pages > Edit
Breadcrumbs::for('order-category.edit', function ($breadcrumbs, $data) {
    $breadcrumbs->parent('order-category.index', $data);
    $breadcrumbs->push('Edit', route('order-category.edit', $data->id));
});



/*
|--------------------------------------------------------------------------
| Site Settings
|--------------------------------------------------------------------------
*/

// Site Setting
Breadcrumbs::for('site-settings.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Site Settings', route('site-settings.index'));
});

/*
|--------------------------------------------------------------------------
| Change Password
|--------------------------------------------------------------------------
*/

// Change Password
Breadcrumbs::for('users.change-password', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Change Password', route('users.change-password'));
});

/*
|--------------------------------------------------------------------------
| Edit Profile
|--------------------------------------------------------------------------
*/

// Edit Profile
Breadcrumbs::for('users.profile', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard.index');
    $breadcrumbs->push('Edit Profile', route('users.edit-profile'));
});



/*
|--------------------------------------------------------------------------
| Front
|--------------------------------------------------------------------------
*/

// Home
Breadcrumbs::for('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('index'));
});

<div class="row">

    @if(can_view_cards('sub_admin_card_count',$dashbord_cards_rights))
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="dashboard-stat dashboard-cart-box-one class green">
                <div class="visual" style="margin-right: 12px;">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {!! isset($subAdmin) ? $subAdmin : 0 !!}
                    </div>
                    <div class="desc">
                        Sub Admins
                    </div>
                </div>
                <a class="more" href="{!! route('sub-admin.index') !!}">
                    See Sub Admins <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    @endif
    @if(can_view_cards('job_type_card_count',$dashbord_cards_rights))
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="dashboard-stat dashboard-cart-box-one class green">
                <div class="visual" style="margin-right: 12px;">
                    <i class="fa fa-tasks"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {!! isset($jobType) ? $jobType : 0 !!}
                    </div>
                    <div class="desc">
                        Job Types
                    </div>
                </div>
                <a class="more" href="{!! route('job-type.index') !!}">
                    See Job Types <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    @endif
    @if(can_view_cards('order_category_card_count',$dashbord_cards_rights))
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="dashboard-stat dashboard-cart-box-one class green">
                <div class="visual" style="margin-right: 12px;">
                    <i class="fa fa-tag"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {!! isset($orderCategory) ? $orderCategory : 0 !!}
                    </div>
                    <div class="desc">
                        Order Categories
                    </div>
                </div>
                <a class="more" href="{!! route('order-category.index') !!}">
                    See Order Categories <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    @endif
</div>

<div class="row">
    @if(can_view_cards('zones_card_count',$dashbord_cards_rights))
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="dashboard-stat dashboard-cart-box-one class green">
                <div class="visual" style="margin-right: 12px;">
                    <i class="fa fa-cog"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {!! isset($zones) ? $zones : 0 !!}
                    </div>
                    <div class="desc">
                        Zones
                    </div>
                </div>
                <a class="more" href="{!! route('zones.index') !!}">
                    See Zones <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    @endif
    @if(can_view_cards('work_time_card_count',$dashbord_cards_rights))
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="dashboard-stat dashboard-cart-box-one class green">
                <div class="visual" style="margin-right: 12px;">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {!! isset($worktime) ? $worktime : 0 !!}
                    </div>
                    <div class="desc">
                        Work Times
                    </div>
                </div>
                <a class="more" href="{!! route('work-time.index') !!}">
                    See Work Times <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    @endif
    @if(can_view_cards('work_type_card_count',$dashbord_cards_rights))
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="dashboard-stat dashboard-cart-box-one class green">
                <div class="visual" style="margin-right: 12px;">
                    <i class="fa fa-briefcase"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {!! isset($worktype) ? $worktype : 0 !!}
                    </div>
                    <div class="desc">
                        Work Types
                    </div>
                </div>
                <a class="more" href="{!! route('work-type.index') !!}">
                    See Work Types <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    @endif
</div>

<div class="row">
    @if(can_view_cards('training_card_count',$dashbord_cards_rights))
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="dashboard-stat dashboard-cart-box-one class green">
                <div class="visual" style="margin-right: 12px;">
                    <i class="fa fa-video-camera"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {!! isset($training) ? $training : 0 !!}
                    </div>
                    <div class="desc">
                        Training Videos
                    </div>
                </div>
                <a class="more" href="{!! route('training.index') !!}">
                    See Training Videos<i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    @endif
    @if(can_view_cards('quiz_management_card_count',$dashbord_cards_rights))
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="dashboard-stat dashboard-cart-box-one class green">
                <div class="visual" style="margin-right: 12px;">
                    <i class="fa fa-question"></i>
                </div>
                <div class="details">
                    <div class="number">
                        {!! isset($quiz) ? $quiz : 0 !!}
                    </div>
                    <div class="desc">
                        Quizzes
                    </div>
                </div>
                <a class="more" href="{!! route('quiz-management.index') !!}">
                    See Quizzes <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>

    @endif

</div>
<div class="clearfix"></div>
<!-- Dashboard cards close -->
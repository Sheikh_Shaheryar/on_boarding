@extends('admin.layouts.app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">{{ $pageTitle ?? '' }} <small></small></h3>
        {{ Breadcrumbs::render('quiz-management.edit', $quizManagement ?? '') }}
        <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">

        @include('admin.partials.errors')

        <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">

                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-edit"></i> {{ $pageTitle ?? '' }}
                    </div>
                </div>

                <div class="portlet-body">

                    <div class="portlet-body">

                        <h4>&nbsp;</h4>

                        <form method="POST"
                              action="{{ route('quiz-management.update', $quizManagement->id) }}"
                              class="form-horizontal" role="form">
                            @csrf
                            @method('PUT')


                            <div class="form-group">
                                <label class="col-md-2 control-label">Question *</label>
                                <div class="col-md-4">
                                <textarea name='question' id='someid' rows="10"
                                          class='form-control form-control col-md-7 col-xs-12'
                                          placeholder='Question..'  required>{{ $quizManagement->question}}</textarea>
                                </div>
                                @if ($errors->has('question'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('question') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group" id='ans1'>
                                <label class="col-md-2 control-label">Options *</label>
                                <div class="col-md-4">
                                    <input type='text' style="margin-bottom: 10px; padding-bottom: 12px; width: 500px;"
                                           name='ans[]' id='ans1' class='form-control' placeholder='Answer1..'
                                           value="{{$dataQuizAnswer[0]['answer']}}" required/>
                                    <input type='hidden' name='old-id[]' value="{{$dataQuizAnswer[0]['id']}}"/>
                                    <br>
                                    <input type='text' style="margin-bottom: 10px; padding-bottom: 12px; width: 500px;"
                                           name='ans[]' id='ans2' class='form-control' placeholder='Answer2..'
                                           value="{{$dataQuizAnswer[1]['answer']}}" required/>
                                    <input type='hidden' name='old-id[]' value="{{$dataQuizAnswer[1]['id']}}"/>
                                    <br>
                                    <input type='text' style="margin-bottom: 10px; padding-bottom: 12px; width: 500px;"
                                           name='ans[]' id='ans3' class='form-control' placeholder='Answer3..'
                                           value="{{$dataQuizAnswer[2]['answer']}}" required/>
                                    <input type='hidden' name='old-id[]' value="{{$dataQuizAnswer[2]['id']}}"/>
                                    <br>
                                    <input type='text' style="margin-bottom: 10px; padding-bottom: 12px; width: 500px;"
                                           name='ans[]' id='ans4' class='form-control' placeholder='Answer4..'
                                           value="{{$dataQuizAnswer[3]['answer']}}" required/>
                                    <input type='hidden' name='old-id[]' value="{{$dataQuizAnswer[3]['id']}}"/>
                                </div>

                                <div style="     margin-left: 25px;"   class="col-md-1">
                                    <label><input type='radio' id='right1' name='right'
                                                  value={{$dataQuizAnswer[0]['id']}}
                                                  @if($dataQuizAnswer[0]['id'] == $quizManagement->correct_answer_id) checked @endif
                                        >a.</label>
                                    <br>
                                    <br>
                                    <br>
                                    <label><input type='radio' id='right2' name='right'
                                                  value={{$dataQuizAnswer[1]['id']}}
                                                  @if($dataQuizAnswer[1]['id'] == $quizManagement->correct_answer_id) checked @endif
                                        >b.</label>
                                    <br>
                                    <br>
                                    <br>
                                    <label><input type='radio' id='right3' name='right'
                                                  value={{$dataQuizAnswer[2]['id']}}
                                                  @if($dataQuizAnswer[2]['id'] == $quizManagement->correct_answer_id) checked @endif
                                        >c.</label>
                                    <br>
                                    <br>
                                    <br>
                                    <label><input type='radio' id='right4' name='right'
                                                  value={{$dataQuizAnswer[3]['id']}}
                                                  @if($dataQuizAnswer[3]['id'] == $quizManagement->correct_answer_id) checked @endif
                                        >d.</label>
                                </div>
                            </div>
                    <div class="form-group" style="    padding-left: 131px;">
                        <div class="col-md-offset-2 col-md-10">
                            <input type="submit" class="btn blue" id="save" value="Save">
                            <input type="button" class="btn black" name="cancel" id="cancel" value="Cancel">
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
@stop

@section('footer-js')
    {{--<script type="text/javascript" src="{!! URL::to('assets/admin/plugins/ckeditor/ckeditor.js') !!}"></script>--}}
    <script src="{{ asset('assets/admin/scripts/core/app.js')}}"></script>
    <script>
        jQuery(document).ready(function () {
            // initiate layout and plugins
            App.init();
            Admin.init();
            $('#cancel').click(function () {
                window.location.href = "{!! URL::route('quiz-management.index') !!}";
            });

        });


    </script>
@stop

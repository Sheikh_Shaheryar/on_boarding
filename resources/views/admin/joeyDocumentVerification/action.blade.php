@if(can_access_route('joey-document-verification.show',$userPermissoins))
<a href="{!! URL::route('joey-document-verification.show', $record->id) !!}" title="Detail"
   class="btn btn-xs btn-primary">
    <i class="fa fa-eye"></i>
</a>
@endif


<select name="status"   class="doc-status-change" id="{{$record->id}}" >
    <option value="0" id="rejected"  {{ $record->profile_status == 0 ? 'Selected' :''}}  >rejected</option>
    <option value="1" id="approved" {{ $record->profile_status == 1 ? 'Selected' :''}}  >approved</option>
    <option value="2" id="pending" {{ $record->profile_status == 2 ? 'Selected' :''}} >pending</option>

</select>

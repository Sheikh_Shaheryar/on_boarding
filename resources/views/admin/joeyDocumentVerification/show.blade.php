@extends('admin.layouts.app')

@section('content')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">{{ $pageTitle ?? '' }} <small></small></h3>
        {{ Breadcrumbs::render('joey-document-verification.show', $joeyDocumentVerification) }}
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->

<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">

        @include('admin.partials.errors')

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-eye"></i> {{ $pageTitle ?? '' }}
                </div>
            </div>

            <div class="portlet-body">

                <h4>&nbsp;</h4>

                <div class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-md-2 control-label"><strong>Name:</strong> </label>
                        <div class="col-md-8">
                            <label class="control-label">{{ $joeyDocumentVerification->display_name}}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label"><strong>Work Permit</strong> </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <img class="img-responsive avatar-view"   src="{{$joeyDocumentVerification->driving_licence_picture}}" style="       margin-left: -2px;height: 78px;" class="avatar" alt="Avatar"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-2 control-label"><strong>Expiry Date Of Work Permit:Licence Permit</strong> </label>

                        <div class="col-md-8">
                            <label class="control-label">{{ $joeyDocumentVerification->work_permit_exp_date }}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label"><strong>Expiry Date Of License</strong> </label>
                        <div class="col-md-8">
                            <label class="control-label">{{ $joeyDocumentVerification->driving_licence_exp_date }}</label>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button class="btn black" id="cancel" onclick="window.location.href = '{!! URL::route('joey-document-verification.index') !!}'"> Back..</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

@section('footer-js')
<script src="{{ asset('assets/admin/scripts/core/app.js') }}"></script>
<script>
jQuery(document).ready(function() {
   // initiate layout and plugins
   App.init();
   Admin.init();
});
</script>
@stop

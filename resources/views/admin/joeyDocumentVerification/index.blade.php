@extends('admin.layouts.app')

@section('css')
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ URL::to('assets/admin/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{!! URL::to('assets/admin/plugins/select2/select2.css') !!}"/>
<link rel="stylesheet" type="text/css" href="{!! URL::to('assets/admin/plugins/select2/select2-metronic.css') !!}"/>
<link href="{{ URL::to('assets/admin/plugins/chosen/chosen.css') }}" rel="stylesheet">
{{--<link rel="stylesheet" href="{!! URL::to('assets/admin/plugins/data-tables/DT_bootstrap.css') !!}"/>--}}
<!-- END PAGE LEVEL STYLES -->
@stop

@section('content')
<!-- BEGIN PAGE HEADER-->
@include('admin.partials.errors')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">{{ $pageTitle ?? '' }} <small></small></h3>
        {{ Breadcrumbs::render('joey-document-verification.index') }}
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- Action buttons Code Start -->
        <div class="col">
            <div class="col-md-12">
                <!-- Add New Button Code Moved Here -->
          {{--      <div class="table-toolbar pull-right">
                    <div class="btn-group">
                        <a href="{!! URL::route('admin.joeyDocumentVerification.create') !!}" id="sample_editable_1_new" class="btn blue">
                            Add <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>--}}
                <!-- Add New Button Code Moved Here -->



            </div>
        </div>

        <div class="box-body row col-sm-9">


            <div class="clearfix"></div>

            <div class="grid-filter">
                <form id="filter-form">
                    <div class="row">
                        <div class=" col-sm-3">
                            <label>Type</label>
                            {!! Form::select('type', $type ?? '', null, ['class' => 'form-control chosen ', 'id' => 'type']) !!}
                        </div>
                        <br>

                        <div class="filter-tools">
                            <div class="btn-group button-container pull-left">
                                <button type="button" class="btn btn-flat btn-default clear-data" style="    margin-top: 6px;
                                                                                                                height: 27px;
                                                                                                                padding-top: 3px;
                                                                                                                /* padding-right: 28px; */
                                                                                                                margin-right: 10px;">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    Clear Filter
                                </button>
                                <button type="button" class="btn btn-flat btn-default filter-data" style="     margin-top: 6px;
                                                                                                                height: 27px;
                                                                                                                padding-top: 3px;
                                                                                                                /* padding-right: 28px; */
                                                                                                                margin-right: 10px;
                                                                                                                width: 116px;">
                                    <i class="fa fa-filter" aria-hidden="true"></i> Filter
                                </button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

            <div class="clearfix"></div>

            <br>


        </div>

        <!-- Action buttons Code End -->

        <div class="clearfix"></div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-list"></i> {{ $pageTitle ?? '' }}
                </div>
            </div>

            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover yajrabox" id="sample_1">
                    <thead>
                        <tr>
                            <th style="width: 5%"  class="text-center">ID</th>
                            <th style="width: 70%" class="text-center ">Name</th>
                            <th style="width: 20%" class="text-center">Status</th>
                            <th style="width: 5%"  class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

@section('footer-js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{!! URL::to('assets/admin/plugins/select2/select2.min.js') !!}"></script>
{{--<script type="text/javascript" src="{!! URL::to('assets/admin/plugins/data-tables/jquery.dataTables.js') !!}"></script>
<script type="text/javascript" src="{!! URL::to('assets/admin/plugins/data-tables/DT_bootstrap.js') !!}"></script>--}}
<!-- END PAGE LEVEL PLUGINS -->
<script src="{{ asset('assets/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/chosen/chosen.jquery.min.js') }}"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/admin/scripts/core/app.js')}}"></script>
<script src="{{ asset('assets/admin/scripts/custom/pages.js') }}"></script>
<script src="{{ asset('assets/admin/scripts/custom/user-administrators.js') }}"></script>
<script>

    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
        Admin.init();
        $('#cancel').click(function() {
            window.location.href = "{!! URL::route('joey-document-verification.index') !!}";
        });
    });
    $(function () {
        appConfig.set('yajrabox.ajax', '{{ route('joey-document-verification.data',$type ?? '') }}');
        appConfig.set('dt.order', [0, 'desc']);
        appConfig.set('yajrabox.ajax.data', function (data) {
            data.type = jQuery('select[name=type]').val();
        });

        appConfig.set('yajrabox.columns', [
            {data: 'id',   orderable: true,   searchable: false, className:'text-center'},
            {data: 'display_name',   orderable: true,   searchable: true, className:'text-center' ,},
            {data: 'status',         orderable: false,    searchable: false, className:'text-center'},
            {data: 'action',            orderable: false,   searchable: false, className:'text-center'}

        ]);
    })

    // document  status change fn
    //$check_status_changer = $('.doc-status-change').length;

    $(document).on('change','.doc-status-change',function () {
       let old_selected_val = ($(this).attr('data-selected-val') >= 0) ? $(this).attr('data-selected-val') : 0 ;
       let selected_val = $(this).val();
       let Id = $(this).attr('id');
       console.log(Id);
       let confirm_val = confirm("Are you sure you want to change the status?");

       // send ajax request if confirm
        if(confirm_val)
        {
            $.ajax({
                type: "GET",
                url: "joeyDocumentVerification/status/update/statusUpdate",
                data:{id:Id,status:selected_val},
                success: function(data){
                    location.reload();
                },error:function (error){
                    alert('Alert Error view console');
                    console.log(error);

                }
            });
        }
        else
        {
            location.reload();
            $(this).val(old_selected_val);
        }
    });

</script>
@stop

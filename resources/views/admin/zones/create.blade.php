@extends('admin.layouts.app')

@section('content')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">{{ $pageTitle ?? '' }} <small></small></h3>
        {{ Breadcrumbs::render('zones.create') }}
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->

<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">

        {{--@include('admin.partials.errors')--}}

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-plus"></i> {{ $pageTitle ?? '' }}
                </div>
            </div>

            <div class="portlet-body">

                <h4>&nbsp;</h4>
                <form method="POST" action="{{ route('zones.store') }}" class="form-horizontal" role="form">
                    @csrf
                    @method('POST')
                    <div class="form-group">
                        <label for="name" class="col-md-2 control-label">Name *</label>
                        <div class="col-md-4">
                            <input type="text" id="name" maxlength="190" name="name" value="{{ old('name') }}" class="form-control" />
                        </div>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="latitude" class="col-md-2 control-label">Latitude *</label>
                        <div class="col-md-4">
                            <input type="text" id="latitude" maxlength="190" name="latitude" value="{{ old('latitude') }}" class="form-control" />
                        </div>
                        @if ($errors->has('latitude'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('latitude') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="longitude" class="col-md-2 control-label">Longitude *</label>
                        <div class="col-md-4">
                            <input type="text" id="longitude" maxlength="190" name="longitude" value="{{ old('longitude') }}" class="form-control" />
                        </div>
                        @if ($errors->has('longitude'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('longitude') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="radius" class="col-md-2 control-label">Radius *</label>
                        <div class="col-md-4">
                            <input type="text" id="radius" maxlength="190" name="radius" value="{{ old('radius') }}" class="form-control" />
                        </div>
                        @if ($errors->has('radius'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('radius') }}</strong>
                                    </span>
                        @endif
                    </div>  <div class="form-group">
                        <label for="time_zone" class="col-md-2 control-label">Time Zones *</label>
                        <div class="col-md-4">
                            <input type="text" id="time_zone" maxlength="190" name="time_zone" value="{{ old('time_zone') }}" class="form-control" />
                        </div>
                        @if ($errors->has('time_zone'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('time_zone') }}</strong>
                                    </span>
                        @endif
                    </div>


                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <input type="submit" class="btn blue" id="save" value="Save">
                            <input type="button" class="btn black" name="cancel" id="cancel" value="Cancel">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

@section('footer-js')
<script type="text/javascript" src="{{ asset('assets/admin/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{!! URL::to('assets/admin/scripts/core/app.js') !!}"></script>
<script>
jQuery(document).ready(function() {

    // initiate layout and plugins
    App.init();
    Admin.init();
    $('#cancel').click(function() {
        window.location.href = "{!! URL::route('zones.index') !!}";
    });
});


</script>
@stop

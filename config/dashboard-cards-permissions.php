<?php


/**
 * Dashboard Card permissions on index page config
 *
 * @author Muhammad Adnan <adnannadeem1994@gmail.com>
 * @date   23/10/2020
 */

return [
    'sub_admin_card_count' => 'Sub Admin Card Count',
    'job_type_card_count' => 'Job Type Card Count',
    'order_category_card_count' => 'Order Category Card Count',
    'zones_card_count' => 'Zone Card Count',
    'work_time_card_count' => 'Work Time Card Count',
    'work_type_card_count' => ' Work Type Card Count',
    'training_card_count' => ' Training Video Card Count',
    'quiz_management_card_count' => 'Quiz Card Count',
];

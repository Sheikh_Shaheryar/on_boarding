<?php


/**
 * Permissions config
 *
 * @author Muhammad Adnan <adnannadeem1994@gmail.com>
 * @date   23/10/2020
 */

return [
    'Roles'=>
        [
            'Role list' => 'role.index',
            'Create' => 'role.create|role.store',
            'Edit' => 'role.edit|role.update',
            'View' => 'role.show',
            'Set permissions' => 'role.set-permissions|role.set-permissions.update',
        ],
    'Sub Admins'=>
        [
            'Sub admin list' => 'sub-admin.index|sub-admin.data',
            'Create' => 'sub-admin.create|sub-admin.store',
            'Edit' => 'sub-admin.edit|sub-admin.update',
            'Status change' => 'sub-admin.active|sub-admin.inactive',
            'View' => 'sub-admin.show',
            'Delete' => 'sub-admin.destroy',
        ],

    'Zones'=>
        [
            'Zones list' => 'zones.index|zones.data',
            'Create' => 'zones.create|zones.store',
            'Edit' => 'zones.edit|zones.update',
            'Delete' => 'zones.destroy',
        ],
    'Work Time'=>
        [
            'Work Time list' => 'work-time.index|work-time.data',
            'Create' => 'work-time.create|work-time.store',
            'Edit' => 'work-time.edit|work-time.update',
            'Delete' => 'work-time.destroy',
        ],
    'Work Type'=>
        [
            'Work Type list' => 'work-type.index|work-type.data',
            'Create' => 'work-type.create|work-type.store',
            'Edit' => 'work-type.edit|work-type.update',
            'Delete' => 'work-type.destroy',
        ],
    'Joey Document Verification'=>
        [
            'Joey Document Verification list' => 'joey-document-verification.index|joey-document-verification.data',
            'View' => 'joey-document-verification.show',
            'Status change' => 'joey-document-verification.statusUpdate',
        ],

    'Training Videos and Documents'=>
        [
            'Training Videos and Documents list' => 'training.index|training.data',
            'Create' => 'training.create|training.store',
            'Delete' => 'training.destroy',
        ],
    'Categories Order Count'=>
        [
            'Categories Order Count list' => 'categores.index|categores.data',
            'Create' => 'categores.create|categores.store',
            'Edit' => 'categores.edit|categores.update',
            'Delete' => 'categores.destroy',
        ],
    'Quizes Management'=>
        [
            'Quizes Management list' => 'quiz-management.index|quiz-management.data',
            'Create' => 'quiz-management.create|quiz-management.store',
            'Edit' => 'quiz-management.edit|quiz-management.update',
            'Delete' => 'quiz-management.destroy',
        ],
    'Job Types '=>
        [
            'Job Types list' => 'job-type.index|job-type.data',
            'Create' => 'job-type.create|job-type.store',
            'Edit' => 'job-type.edit|job-type.update',
            'Delete' => 'job-type.destroy',
        ],

    'Joey Checklists '=>
        [
            'Joey Checklists list' => 'joey-checklist.index|joey-checklist.data',
            'Create' => 'joey-checklist.create|joey-checklist.store',
            'Edit' => 'joey-checklist.edit|joey-checklist.update',
            'Delete' => 'joey-checklist.destroy',
        ],
    'Basic Vendors '=>
        [
            'Basic Vendors list' => 'basic-vendor.index|basic-vendor.data',
            'Create' => 'basic-vendor.create|basic-vendor.store',
            'Delete' => 'basic-vendor.destroy',
        ],
    'Basic Categories'=>
        [
            'Basic Categories list' => 'basic-category.index|basic-category.data',
            'Create' => 'basic-category.create|basic-category.store',
            'Delete' => 'basic-category.destroy',
        ],
    'Vendors Score'=>
        [
            'Vendors Score list' => 'vendor-score.index|vendor-score.data',
            'Create' => 'vendor-score.create|vendor-score.store',
            'Edit' => 'vendor-score.edit|vendor-score.update',
            'Delete' => 'vendor-score.destroy',
        ],

    'Categories Score'=>
        [
            'Categories Score list' => 'category-score.index|category-score.data',
            'Create' => 'category-score.create|category-score.store',
            'Edit' => 'category-score.edit|category-score.update',
            'Delete' => 'category-score.destroy',
        ],
    'Vendors Order Count'=>
        [
            'Vendors Order Count list' => 'vendors.index|vendors.data',
            'Create' => 'vendors.create|vendors.store',
            'Edit' => 'vendors.edit|vendors.update',
            'Delete' => 'vendors.destroy',
        ],

    'Order Categories'=>
        [
            'Order Categories list' => 'order-category.index|order-category.data',
            'Create' => 'order-category.create|order-category.store',
            'Edit' => 'order-category.edit|order-category.update',
            'Delete' => 'order-category.destroy',
        ],
    'Order Categories'=>
        [
            'Order Categories list' => 'order-category.index|order-category.data',
            'Create' => 'order-category.create|order-category.store',
            'Edit' => 'order-category.edit|order-category.update',
            'Delete' => 'order-category.destroy',
        ],
    'Setting'=>
        [
            'Change Password' => 'users.change-password',
            'Edit Profile' => 'users.edit-profile',
        ],
];

<?php

namespace App\Services\Menu;

use Illuminate\Support\Facades\Auth;
use Spatie\Menu\Laravel\Link;
use Spatie\Menu\Laravel\Menu;

/**
 * Class AdminMenu
 *
 * @author Muzafar Ali Jatoi <muzfr7@gmail.com>
 * @date   23/9/18
 */
class AdminMenu
{
    public function register()
    {
        Menu::macro('admin', function () {

            /*getting user permissions*/

            $userPermissoins = Auth::user()->getPermissions();

            return Menu::new()

                ->addClass('page-sidebar-menu')
                ->setAttribute('data-keep-expanded', 'false')
                ->setAttribute('data-auto-scroll', 'true')
                ->setAttribute('data-slide-speed', '200')
                ->html('<div class="sidebar-toggler hidden-phone"></div>')

                ->add(Link::toRoute(
                    'dashboard.index',
                    '<i class="fa fa-home"></i> <span class="title">Dashboard</span>'
                )
                ->addParentClass('start'))

                ->submenuIf(can_access_route(['role.index','role.create'],$userPermissoins),'
                    <a href="javascript:;">
                        <i class="fa fa-exclamation-circle"></i>
                        <span class="title">Roles </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('role.index',$userPermissoins),
                            Link::toRoute('role.index', '<span class="title">Role List</span>'))
                        ->addIf(can_access_route('role.create',$userPermissoins),
                            Link::toRoute('role.create', '<span class="title">Add Role</span>'))
                )

                ->submenuIf(can_access_route(['sub-admin.index','sub-admin.create'],$userPermissoins),'
                    <a href="javascript:;">
                        <i class="fa fa-users"></i>
                        <span class="title">Sub Admins </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('sub-admin.index',$userPermissoins),
                            Link::toRoute('sub-admin.index', '<span class="title">Sub Admins List</span>'))
                        ->addIf(can_access_route('sub-admin.create',$userPermissoins),
                            Link::toRoute('sub-admin.create', '<span class="title">Add Sub Admin</span>'))
                )


                ->submenuIf(can_access_route(['zones.index','zones.create'],$userPermissoins),'
                    <a href="javascript:;">
                        <i class="fa fa-cog"></i>
                        <span class="title">Zones </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('zones.index',$userPermissoins),
                            Link::toRoute('zones.index', '<span class="title">Zones List</span>'))
                        ->addIf(can_access_route('zones.create',$userPermissoins),
                            Link::toRoute('zones.create', '<span class="title">Add Zone</span>'))
                )


                ->submenuIf(can_access_route(['work-time.index','work-time.create'],$userPermissoins),'
                    <a href="javascript:;">
                        <i class="fa fa-clock-o"></i>
                        <span class="title">Work Time</span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('work-time.index',$userPermissoins),
                            Link::toRoute('work-time.index', '<span class="title">Work Time List</span>'))
                        ->addIf(can_access_route('work-time.create',$userPermissoins),
                            Link::toRoute('work-time.create', '<span class="title">Add Work Time</span>'))
                )


                ->submenuIf(can_access_route(['work-type.index', 'work-type.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-briefcase"></i>
                        <span class="title">Work Types </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('work-type.index', $userPermissoins),
                            Link::toRoute('work-type.index', '<span class="title">Work Types List</span>'))
                        ->addIf(can_access_route('work-type.create', $userPermissoins),
                            Link::toRoute('work-type.create', '<span class="title">Add Work Type</span>'))
                )

                ->submenuIf(can_access_route(['joey-document-verification.index', 'joey-document-verification.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-certificate"></i>
                        <span class="title">Joey Documents Verification </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('joey-document-verification.index', $userPermissoins),
                            Link::toRoute('joey-document-verification.index', '<span class="title">Joey Documents Verification List</span>'))

                )


                ->submenuIf(can_access_route(['training.index', 'training.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-video-camera"></i>
                        <span class="title">Training Videos and Documents </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('training.index', $userPermissoins),
                            Link::toRoute('training.index', '<span class="title">Training Videos and Documents List</span>'))
                        ->addIf(can_access_route('training.create', $userPermissoins),
                            Link::toRoute('training.create', '<span class="title">Add Training Videos and Document</span>'))
                )


                ->submenuIf(can_access_route(['categores.index', 'categores.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-tag"></i>
                        <span class="title">Categories Order Count </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('categores.index', $userPermissoins),
                            Link::toRoute('categores.index', '<span class="title">Categories Order Count List</span>'))
                        ->addIf(can_access_route('categores.create', $userPermissoins),
                            Link::toRoute('categores.create', '<span class="title">Add Categories Order Count</span>'))
                )


                ->submenuIf(can_access_route(['quiz-management.index', 'quiz-management.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-question"></i>
                        <span class="title">Quizes Management </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('quiz-management.index', $userPermissoins),
                            Link::toRoute('quiz-management.index', '<span class="title">Quizes Management List</span>'))
                        ->addIf(can_access_route('quiz-management.create', $userPermissoins),
                            Link::toRoute('quiz-management.create', '<span class="title">Add Quizes Management</span>'))
                )


                ->submenuIf(can_access_route(['order-category.index', 'order-category.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span class="title">Order Categories</span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('order-category.index', $userPermissoins),
                            Link::toRoute('order-category.index', '<span class="title">Order Categories List</span>'))
                        ->addIf(can_access_route('order-category.create', $userPermissoins),
                            Link::toRoute('order-category.create', '<span class="title">Add Order Category</span>'))
                )

                ->submenuIf(can_access_route(['job-type.index', 'job-type.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span class="title">Job Types </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('job-type.index', $userPermissoins),
                            Link::toRoute('job-type.index', '<span class="title">Job Types List</span>'))
                        ->addIf(can_access_route('job-type.create', $userPermissoins),
                            Link::toRoute('job-type.create', '<span class="title">Add Job Type</span>'))
                )

                ->submenuIf(can_access_route(['joey-checklist.index', 'joey-checklist.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-check"></i>
                        <span class="title">Joey Checklists </span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('joey-checklist.index', $userPermissoins),
                            Link::toRoute('joey-checklist.index', '<span class="title">Joey Checklists List</span>'))
                        ->addIf(can_access_route('joey-checklist.create', $userPermissoins),
                            Link::toRoute('joey-checklist.create', '<span class="title">Add Joey Checklist</span>'))
                )

                ->submenuIf(can_access_route(['basic-vendor.index', 'basic-vendor.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-barcode"></i>
                        <span class="title">Basic Vendors</span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('basic-vendor.index', $userPermissoins),
                            Link::toRoute('basic-vendor.index', '<span class="title">Basic Vendors List</span>'))
                        ->addIf(can_access_route('basic-vendor.create', $userPermissoins),
                            Link::toRoute('basic-vendor.create', '<span class="title">Add Basic Vendor</span>'))
                )


                ->submenuIf(can_access_route(['basic-category.index', 'basic-category.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-certificate"></i>
                        <span class="title">Basic Categories</span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('basic-category.index', $userPermissoins),
                            Link::toRoute('basic-category.index', '<span class="title">Basic Categories</span>'))
                        ->addIf(can_access_route('basic-category.create', $userPermissoins),
                            Link::toRoute('basic-category.create', '<span class="title">Add Basic Category</span>'))
                )

                ->submenuIf(can_access_route(['vendor-score.index', 'vendor-score.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-sort-numeric-asc"></i>
                        <span class="title">Vendors Score</span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('vendor-score.index', $userPermissoins),
                            Link::toRoute('vendor-score.index', '<span class="title">Vendors Score</span>'))
                        ->addIf(can_access_route('vendor-score.create', $userPermissoins),
                            Link::toRoute('vendor-score.create', '<span class="title">Add Vendor Score</span>'))
                )


                ->submenuIf(can_access_route(['category-score.index', 'category-score.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-sort-numeric-asc"></i>
                        <span class="title">Categories Score</span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('category-score.index', $userPermissoins),
                            Link::toRoute('category-score.index', '<span class="title">Categories Score</span>'))
                        ->addIf(can_access_route('category-score.create', $userPermissoins),
                            Link::toRoute('category-score.create', '<span class="title">Add Category Score</span>'))
                )


                ->submenuIf(can_access_route(['vendors.index', 'vendors.create'], $userPermissoins), '
                    <a href="javascript:;">
                        <i class="fa fa-tasks"></i>
                        <span class="title">Vendors Order Count</span>
                        <span class="arrow open"></span>
                        <!--<span class="selected"></span>-->
                    </a>
                    ',
                    Menu::new()
                        ->addClass('sub-menu')
                        ->addIf(can_access_route('vendors.index', $userPermissoins),
                            Link::toRoute('vendors.index', '<span class="title">Vendors Order Count</span>'))
                        ->addIf(can_access_route('vendors.create', $userPermissoins),
                            Link::toRoute('vendors.create', '<span class="title">Add Vendor Order Count</span>'))
                )





                ->addIf(can_access_route('users.change-password', $userPermissoins),(Link::toRoute(
                    'users.change-password',
                    '<i class="fa fa-lock"></i> <span class="title">Change Password</span>'
                )))

                ->add(Link::toRoute(
                    'logout',
                    '<i class="fa fa-sign-out"></i> <span class="title">Logout</span>'
                )
                    ->setAttribute('id', 'leftnav-logout-link'))
                ->setActiveFromRequest();
        });
    }
}

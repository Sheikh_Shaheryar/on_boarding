<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use App\Models\Store;
use App\Models\SideBanner;
use App\Models\Event;
use App\Models\Offer;

/**
 * Class ViewComposerServiceProvider
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   30/11/18
 */
class ViewComposerServiceProvider extends ServiceProvider
{


    public function boot()
    {
        $this->composeAdminPages();
        view()->composer('*', function ($view)
        {
            $date = date('Y-m-d');

            /*setting default variables */
            $userPermissoins = [];
            $dashbord_cards_rights = false;

            /*checking user is login or not */
            if(Auth::check())
            {
                $auth_user = Auth::user();
                $userPermissoins = $auth_user->getPermissions();
                $dashbord_cards_rights = $auth_user->DashboardCardRightsArray();
            }

            /*composing data to all views */
            $view->with(compact(
                'userPermissoins',
                'dashbord_cards_rights'
            ));

        });
    }

    public function register()
    {
        //
    }

    /**
     * Compose the admin pages
     *
     * e-g: admin page titles etc.
     */
    private function composeAdminPages()
    {
        /*
         * Dashboard
         */
        view()->composer('admin.dashboard.index', function ($view) {
            $view->with(['pageTitle' => 'Dashboard']);
        });


        /*
       * role & permissions
       */

        view()->composer('admin.role.index', function ($view) {
            $view->with(['pageTitle' => 'Role List']);
        });
        view()->composer('admin.role.create', function ($view) {
            $view->with(['pageTitle' => 'Role Create']);
        });
        view()->composer('admin.role.show', function ($view) {
            $view->with(['pageTitle' => 'Role View']);
        });
        view()->composer('admin.role.edit', function ($view) {
            $view->with(['pageTitle' => 'Role Edit']);
        });
        view()->composer('admin.role.set-permissions', function ($view) {
            $view->with(['pageTitle' => 'Set Role Permissions']);
        });

        /*
        * Sub Admin
        */

        view()->composer('admin.subAdmin.index', function ($view) {
            $view->with(['pageTitle' => 'Sub Admins List']);
        });
        view()->composer('admin.subAdmin.create', function ($view) {
            $view->with(['pageTitle' => 'Sub Admin Create']);
        });
        view()->composer('admin.subAdmin.show', function ($view) {
            $view->with(['pageTitle' => 'Sub Admin View']);
        });
        view()->composer('admin.subAdmin.edit', function ($view) {
            $view->with(['pageTitle' => 'Sub Admin Edit']);
        });

        /*
        * Zone Index
        */

        view()->composer('admin.zones.index', function ($view) {
            $view->with(['pageTitle' => 'Zones List']);
        });
        view()->composer('admin.zones.create', function ($view) {
            $view->with(['pageTitle' => 'Zone Create']);
        });
        view()->composer('admin.zones.show', function ($view) {
            $view->with(['pageTitle' => 'Zone View']);
        });
        view()->composer('admin.zones.edit', function ($view) {
            $view->with(['pageTitle' => 'Zone Edit']);
        });

        /*
        * WORK TIME
        */

        view()->composer('admin.workTime.index', function ($view) {
            $view->with(['pageTitle' => 'Work Time List']);
        });
        view()->composer('admin.workTime.create', function ($view) {
            $view->with(['pageTitle' => 'Work Time Create']);
        });
        view()->composer('admin.workTime.show', function ($view) {
            $view->with(['pageTitle' => 'Work Time View']);
        });
        view()->composer('admin.workTime.edit', function ($view) {
            $view->with(['pageTitle' => 'Work Time Edit']);
        });

        /*
        * WORK TYPE
        */

        view()->composer('admin.workType.index', function ($view) {
            $view->with(['pageTitle' => 'Work Types List']);
        });
        view()->composer('admin.workType.create', function ($view) {
            $view->with(['pageTitle' => 'Work Type Create']);
        });
        view()->composer('admin.workType.show', function ($view) {
            $view->with(['pageTitle' => 'Work Type View']);
        });
        view()->composer('admin.workType.edit', function ($view) {
            $view->with(['pageTitle' => 'Work Type Edit']);
        });

        /*
        * JOEY DOCUMENT VERIFICATION
        */

        view()->composer('admin.joeyDocumentVerification.index', function ($view) {
            $view->with(['pageTitle' => 'Joey Documents Verification List']);
        });
        view()->composer('admin.joeyDocumentVerification.create', function ($view) {
            $view->with(['pageTitle' => 'Joey Document Verification Create']);
        });
        view()->composer('admin.joeyDocumentVerification.show', function ($view) {
            $view->with(['pageTitle' => 'Joey Document Verification View']);
        });
        view()->composer('admin.joeyDocumentVerification.edit', function ($view) {
            $view->with(['pageTitle' => 'Joey Document Verification Edit']);
        });

        /*
        * Training Videos and documents
        */

        view()->composer('admin.training.index', function ($view) {
            $view->with(['pageTitle' => 'Training Videos And Documents List']);
        });
        view()->composer('admin.training.create', function ($view) {
            $view->with(['pageTitle' => 'Training Video And Document Create']);
        });
        view()->composer('admin.training.show', function ($view) {
            $view->with(['pageTitle' => 'Training Video And Document View']);
        });
        view()->composer('admin.training.edit', function ($view) {
            $view->with(['pageTitle' => 'Training Video And Document Edit']);
        });

        /*
        * Categories
        */

        view()->composer('admin.categores.index', function ($view) {
            $view->with(['pageTitle' => 'Categories Order Count List']);
        });
        view()->composer('admin.categores.create', function ($view) {
            $view->with(['pageTitle' => 'Category Create']);
        });
        view()->composer('admin.categores.show', function ($view) {
            $view->with(['pageTitle' => 'Category View']);
        });
        view()->composer('admin.categores.edit', function ($view) {
            $view->with(['pageTitle' => 'Category Edit']);
        });

        /*
        * Quiz
        */

        view()->composer('admin.quizManagement.index', function ($view) {
            $view->with(['pageTitle' => 'Quizes List']);
        });
        view()->composer('admin.quizManagement.create', function ($view) {
            $view->with(['pageTitle' => 'Quiz Create']);
        });
        view()->composer('admin.quizManagement.show', function ($view) {
            $view->with(['pageTitle' => 'Quiz View']);
        });
        view()->composer('admin.quizManagement.edit', function ($view) {
            $view->with(['pageTitle' => 'Quiz Edit']);
        });

        /*
        * Job Type
        */

        view()->composer('admin.jobType.index', function ($view) {
            $view->with(['pageTitle' => 'Job Types List']);
        });
        view()->composer('admin.jobType.create', function ($view) {
            $view->with(['pageTitle' => 'Job Type Create']);
        });
        view()->composer('admin.jobType.show', function ($view) {
            $view->with(['pageTitle' => 'Job Type View']);
        });
        view()->composer('admin.jobType.edit', function ($view) {
            $view->with(['pageTitle' => 'Job Type Edit']);
        });


        /*
        * Order Category
        */

        view()->composer('admin.orderCategory.index', function ($view) {
            $view->with(['pageTitle' => 'Order Categories List']);
        });
        view()->composer('admin.orderCategory.create', function ($view) {
            $view->with(['pageTitle' => 'Order Category Create']);
        });
        view()->composer('admin.orderCategory.show', function ($view) {
            $view->with(['pageTitle' => 'Order Category View']);
        });
        view()->composer('admin.orderCategory.edit', function ($view) {
            $view->with(['pageTitle' => 'Order Category Edit']);
        });

        /*
                 * Joey Check List
                 */

        view()->composer('admin.joeyChecklist.index', function ($view) {
            $view->with(['pageTitle' => 'Joeyes Checklist']);
        });
        view()->composer('admin.joeyChecklist.create', function ($view) {
            $view->with(['pageTitle' => 'Joey Checklist Create']);
        });
        view()->composer('admin.joeyChecklist.show', function ($view) {
            $view->with(['pageTitle' => 'Joey Checklist  View']);
        });
        view()->composer('admin.joeyChecklist.edit', function ($view) {
            $view->with(['pageTitle' => 'Joey Checklist Edit']);
        });


        /*
       * vendors order count
       */

        view()->composer('admin.vendors.index', function ($view) {
            $view->with(['pageTitle' => 'Vendors Order Counts']);
        });
        view()->composer('admin.vendors.create', function ($view) {
            $view->with(['pageTitle' => 'Vendors Order Count Create']);
        });
        view()->composer('admin.vendors.show', function ($view) {
            $view->with(['pageTitle' => 'Vendors Order Count View']);
        });
        view()->composer('admin.vendors.edit', function ($view) {
            $view->with(['pageTitle' => 'Vendors Order CountEdit']);
        });

        /*
                * Basic Vendor
                */

        view()->composer('admin.basicVendor.index', function ($view) {
            $view->with(['pageTitle' => 'Basic Vendors List']);
        });
        view()->composer('admin.basicVendor.create', function ($view) {
            $view->with(['pageTitle' => 'Basic Vendor Create']);
        });


        /*
        * Basic category
        */

        view()->composer('admin.basicCategory.index', function ($view) {
            $view->with(['pageTitle' => 'Basic Categories List']);
        });
        view()->composer('admin.basicCategory.create', function ($view) {
            $view->with(['pageTitle' => 'Basic Category Create']);
        });


        /*
       * Vendor Score
       */

        view()->composer('admin.vendorScore.index', function ($view) {
            $view->with(['pageTitle' => 'Vendors Score List']);
        });
        view()->composer('admin.vendorScore.create', function ($view) {
            $view->with(['pageTitle' => 'Vendor Score Create']);
        });
        view()->composer('admin.vendorScore.show', function ($view) {
            $view->with(['pageTitle' => 'Vendor Score View']);
        });
        view()->composer('admin.vendorScore.edit', function ($view) {
            $view->with(['pageTitle' => 'Vendor Score Edit']);
        });


        /*
        * Category Score
        */

        view()->composer('admin.categoryScore.index', function ($view) {
            $view->with(['pageTitle' => 'Categories Score List']);
        });
        view()->composer('admin.categoryScore.create', function ($view) {
            $view->with(['pageTitle' => 'Category Score Create']);
        });
        view()->composer('admin.categoryScore.show', function ($view) {
            $view->with(['pageTitle' => 'Category Score View']);
        });
        view()->composer('admin.categoryScore.edit', function ($view) {
            $view->with(['pageTitle' => 'Category Score Edit']);
        });



        /*
         * Change Password
         */
        view()->composer('admin.users.changePassword', function ($view) {
            $view->with(['pageTitle' => 'Change Password']);
        });

        /*
         * Change Password
         */
        view()->composer('admin.users.profile', function ($view) {
            $view->with(['pageTitle' => 'Edit Profile']);
        });

    }
}

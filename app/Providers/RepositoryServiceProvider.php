<?php

namespace App\Providers;

use App\Repositories\Interfaces\PropertyRepositoryInterface;
use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
class RepositoryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    /**
     * Bind the interface to an implementation repository class
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Interfaces\AdminRepositoryInterface', 'App\Repositories\AdminRepository');
        $this->app->bind('App\Repositories\Interfaces\SiteSettingRepositoryInterface', 'App\Repositories\SiteSettingRepository');
        $this->app->bind('App\Repositories\Interfaces\UserRepositoryInterface', 'App\Repositories\UserRepository');
        $this->app->bind('App\Repositories\Interfaces\JobTypeRepositoryInterface', 'App\Repositories\JobTypeRepository');
        $this->app->bind('App\Repositories\Interfaces\OrderCategoryRepositoryInterface', 'App\Repositories\OrderCategoryRepository');
        $this->app->bind('App\Repositories\Interfaces\ZoneRepositoryInterface', 'App\Repositories\ZoneRepository');
        $this->app->bind('App\Repositories\Interfaces\WorkTimeRepositoryInterface', 'App\Repositories\WorkTimeRepository');
        $this->app->bind('App\Repositories\Interfaces\WorkTypeRepositoryInterface', 'App\Repositories\WorkTypeRepository');
        $this->app->bind('App\Repositories\Interfaces\JoeyDocumentVerificationRepositoryInterface', 'App\Repositories\JoeyDocumentVerificationRepository');
        $this->app->bind('App\Repositories\Interfaces\TrainingRepositoryInterface', 'App\Repositories\TrainingRepository');
        $this->app->bind('App\Repositories\Interfaces\CategoresRepositoryInterface', 'App\Repositories\CategoresRepository');
        $this->app->bind('App\Repositories\Interfaces\QuizQuestionRepositoryInterface', 'App\Repositories\QuizQuestionRepository');
        $this->app->bind('App\Repositories\Interfaces\QuizAnswerRepositoryInterface', 'App\Repositories\QuizAnswerRepository');
        $this->app->bind('App\Repositories\Interfaces\VendorsRepositoryInterface', 'App\Repositories\VendorsRepository');
        $this->app->bind('App\Repositories\Interfaces\JoeyChecklistRepositoryInterface', 'App\Repositories\JoeyChecklistRepository');
        $this->app->bind('App\Repositories\Interfaces\BasicVendorRepositoryInterface', 'App\Repositories\BasicVendorRepository');
        $this->app->bind('App\Repositories\Interfaces\BasicCategoryRepositoryInterface', 'App\Repositories\BasicCategoryRepository');
        $this->app->bind('App\Repositories\Interfaces\VendorRepositoryInterface', 'App\Repositories\VendorRepository');
        $this->app->bind('App\Repositories\Interfaces\CategoriesRepositoryInterface', 'App\Repositories\CategoriesRepository');
        $this->app->bind('App\Repositories\Interfaces\RoleRepositoryInterface','App\Repositories\RolesRepository');
    }
}

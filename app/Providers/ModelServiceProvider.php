<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class ModelServiceProvider
 *
 * @author Ghulam Mustafa <ghulam.mustafa@vservices.com>
 * @date   29/11/18
 */
class ModelServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    /**
     * Bind the interface to an implementation model class
     */
    public function register()
    {
        $this->app->bind('App\Models\Interfaces\UserInterface', 'App\Models\User');
        $this->app->bind('App\Models\Interfaces\SiteSettingInterface', 'App\Models\SiteSetting');
        $this->app->bind('App\Models\Interfaces\OrderCategoryInterface', 'App\Models\OrderCategory');
        $this->app->bind('App\Models\Interfaces\JobTypeInterface', 'App\Models\JobType');
        $this->app->bind('App\Models\Interfaces\ZoneInterface', 'App\Models\Zone');
        $this->app->bind('App\Models\Interfaces\WorkTimeInterface', 'App\Models\WorkTime');
        $this->app->bind('App\Models\Interfaces\WorkTypeInterface', 'App\Models\WorkType');
        $this->app->bind('App\Models\Interfaces\SiteSettingInterface', 'App\Models\SiteSetting');
        $this->app->bind('App\Models\Interfaces\JoeyDocumentVerificationInterface', 'App\Models\JoeyDocumentVerification');
        $this->app->bind('App\Models\Interfaces\TrainingInterface', 'App\Models\Training');
        $this->app->bind('App\Models\Interfaces\CategoresInterface', 'App\Models\Categores');
        $this->app->bind('App\Models\Interfaces\CategoriesInterface', 'App\Models\Categories');
        $this->app->bind('App\Models\Interfaces\QuizQuestionInterface', 'App\Models\QuizQuestion');
        $this->app->bind('App\Models\Interfaces\QuizAnswerInterface', 'App\Models\QuizAnswer');
        $this->app->bind('App\Models\Interfaces\VendorInterface', 'App\Models\Vendor');
        $this->app->bind('App\Models\Interfaces\VendorsInterface', 'App\Models\Vendors');
        $this->app->bind('App\Models\Interfaces\JoeyChecklistInterface', 'App\Models\JoeyChecklist');
        $this->app->bind('App\Models\Interfaces\BasicVendorInterface', 'App\Models\BasicVendor');
        $this->app->bind('App\Models\Interfaces\BasicCategoryInterface', 'App\Models\BasicCategory');
        $this->app->bind('App\Models\Interfaces\RoleInterface', 'App\Models\Roles');
        $this->app->bind('App\Models\Interfaces\PermissionsInterface', 'App\Models\Permissions');

    }
}

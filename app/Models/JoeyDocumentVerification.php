<?php

namespace App\Models;

use App\Models\Interfaces\JoeyDocumentVerificationInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JoeyDocumentVerification extends Model implements JoeyDocumentVerificationInterface
{

    public $table = 'joeys';

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'plan_id',
        'first_name',
        'last_name',
        'nickname',
        'display_name',
        'email',
        'password',
        'address',
        'suite',
        'buzzer',
        'city_id',
        'state_id',
        'country_id',
        'postal_code',
        'phone',
        'image_path',
        'image',
        'about_yourself',
        'about',
        'preferred_zone',
        'hear_from',
        'is_newsletter',
        'is_enabled',
        'created_at',
        'updated_at',
        'deleted_at',
        'vehicle_id',
        'comdata_emp_num',
        'comdata_cc_num',
        'comdata_cc_num_2',
        'pwd_reset_token',
        'pwd_reset_token_expiry',
        'is_busy',
        'current_location_id',
        'email_verify_token',
        'is_online',
        'balance',
        'location_id',
        'hst_number',
        'rbc_deposit_number',
        'cash_on_hand',
        'timezone',
        'work_type',
        'contact_time',
        'interview_time',
        'has_bag',
        'is_backcheck',
        'on_duty',
        'preferred_zone_id',
        'shift_amount_due',
        'is_on_shift',
        'api_key',
        'is_itinerary',
        'hub_id',
        'vendor_id',
        'category_id',
        'merchant_id',
        'cirminal_status',
        'driving_licence_status',
        'work_permit_status',
        'driving_licence_picture',
        'driving_licence_exp_date',
        'work_permit_image',
        'work_permit_exp_date',
        'document_status',
        'quiz_status',
        'profile_status',
        'is_background_check',
        'unit_number',
        'hub_joey_type',
        'has_route',



    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getDisplayNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }
    public function setDisplayNameAttribute($value)
    {

        $this->attributes['display_name'] =$value;
    }

    public function getFirstNameAttribute($value)
    {
        return ucfirst($value);
    }
    public function setFirstNameAttribute($value)
    {

        $this->attributes['first_name'] =$value;
    }


    public function getLastNameAttribute($value)
    {
        return ucfirst($value);
    }
    public function setLastNameAttribute($value)
    {

        $this->attributes['last_name'] =$value;
    }
}

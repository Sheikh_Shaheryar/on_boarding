<?php

namespace App\Models;

use App\Models\Interfaces\CategoresInterface;


use App\Models\Interfaces\VendorsInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendors extends Model implements VendorsInterface
{

    public $table = 'vendor_count';

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'vendor_id',
        'order_count',
        'deleted_at',
        'created_at',
        'updated_at',

    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function vendor() {
        return $this->belongsTo(vendor::class,'vendor_id','id');
    }




}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\JoeyDocumentVerification;
use App\Repositories\Interfaces\JoeyDocumentVerificationRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JoeyDocumentVerificationController extends Controller
{
    private $joeyDocumentVerificationRepository;

    /**
     * Create a new controller instance.
     *
     * @param JoeyDocumentVerificationRepositoryInterface $joeyDocumentVerificationRepository
     */


    public function __construct(JoeyDocumentVerificationRepositoryInterface $joeyDocumentVerificationRepository)
    {
        $this->middleware('auth:admin');
        parent::__construct();
        $this->joeyDocumentVerificationRepository = $joeyDocumentVerificationRepository;
    }


    public function index()
    {

        $type = ['0' => 'Select Type', '1' => 'Work permit', '2' => 'license',];
        return view('admin.joeyDocumentVerification.index',compact('type'));
    }

    public function data(DataTables $datatables, Request $request) : JsonResponse
    {

        $type = $request->query();

        if(!$type['type']==0) {
       if ($type['type'] == 1) {

                $type1 = 'work_permit_image';

            } else {

                $type1 = 'driving_licence_picture';

            }
            $query= JoeyDocumentVerification::whereNotNull($type1);
        }
        else{
                $query  =JoeyDocumentVerification::query();
        }
        return $datatables->eloquent($query,$type)

            ->setRowId(static function ($record) {
                return $record->id;
            })

            ->addColumn('status', static function ($record) {
                return backend_view('joeyDocumentVerification.status', compact('record') );
            })
            ->addColumn('action', static function ($record) {
                return backend_view('joeyDocumentVerification.action', compact('record') );
            })
            ->rawColumns(['is_active'])
            ->make(true);
    }

    public function statusUpdate(Request $request){

        $statusData=$request->all();

        $updateStatus = [
            'profile_status' => $statusData['status'],
        ];
        $this->joeyDocumentVerificationRepository->update($statusData['id'], $updateStatus);
        return 'true';

    }


    public function show(JoeyDocumentVerification $joeyDocumentVerification)
    {

        return view('admin.joeyDocumentVerification.show', compact('joeyDocumentVerification')
        );
    }




}

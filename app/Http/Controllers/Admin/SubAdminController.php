<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreSubAdminRequest;
use App\Http\Requests\Admin\UpdateSubAdminRequest;
use App\Mail\WelcomeMail;
use App\Models\OrderCategory;
use App\Models\Roles;
use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use DB;
class SubAdminController extends Controller
{

    private $userRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->middleware('auth:admin');
        parent::__construct();

        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return backend_view('subAdmin.index');
    }

    public function data(DataTables $datatables, Request $request): JsonResponse
    {
        $query = User::where('id','!=',auth()->user()->id);//->NotAdmin();



        return $datatables->eloquent($query)

            ->setRowId(static function ($record) {

                return $record->id;
            })
            ->editColumn('created_at', static function ($record) {
                return $record->created_at;
            })
            ->editColumn('phone', static function ($record) {
                return $record->phone_formatted;
            })
            ->editColumn('status', static function ($record) {

                return backend_view('subAdmin.status', compact('record') );

            })
                ->addColumn('image', static function ($record) {

                    return backend_view('subAdmin.image', compact('record') );

                })
            ->addColumn('action', static function ($record) {
                return backend_view('subAdmin.action', compact('record'));
            })
            ->rawColumns(['status', 'phone','link', 'action'])
            ->make(true);
    }


    public function active(User $record)
    {
        $record->activate();
        return redirect()
            ->route('sub-admin.index')
            ->with('success', 'Sub Admin has been Active successfully!');
    }

    public function inactive(User $record)
    {
        $record->deactivate();
        return redirect()
            ->route('sub-admin.index')
            ->with('success', 'Sub Admin has been Inactive successfully!');
    }

    /**
     * Order Category create form open.
     *
     */
    public function create()
    {
        $role_list  = Roles::NotAdminRole()->get();
        return view('admin.subAdmin.create',compact('role_list'));
    }

    public function store(StoreSubAdminRequest $request)
    {

        $data = $request->except(
            [
                '_token',
                '_method',
            ]
        );
        $data = $request->all();

        $file = $request->file('upload_file');
        if ($request->hasfile('upload_file')) {
            $file = $request->file('upload_file');
            $fileName = $file->getClientOriginalName();
            $file->move(backendUserFile(), $file->getClientOriginalName());

            $data['upload_file'] = $fileName;

        }
        $createRecord = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'full_name' => $data['first_name'] . ' ' . $data['last_name'],
            'user_name' => strtolower($data['first_name'] . $data['last_name']),
            'email' => $data['email'],
            'phone' => phoneFormat($data['phone']),
            'address' => $data['address'] ?? '',
            'password' => \Hash::make($data['password']),
            'role_id' =>$data['role'],
            'status' => 1,
            'userType' => 'admin',
            'profile_picture' =>url(backendUserFile() . $file->getClientOriginalName()),

        ];


        $user=$this->userRepository->create($createRecord);
       // Mail::to($data['email'])->send(new WelcomeMail());


        $user->sendWelcomeEmail();



        /*
                $token = Str::random(64);

                    DB::table(config('auth.passwords.users.table'))->insert([
                    'email' => $data['email'],
                    'token' => $token
                ]);

                $resetUrl= url(config('app.url').route('password.reset', $token, false));

                Mail::to($data['email'])->send(new WelcomeMail($this, $resetUrl));

        */
                return redirect()
                    ->route('sub-admin.index')
                    ->with('success', 'Sub Admin added successfully.');

            }

            /**
             * Display the specified resource.
             *
             */
    public function show( $subadmin)
    {

        $id=base64_decode ($subadmin);

         $sub_admin=User ::find($id);
/*User ::find($id);*/

        return view('admin.subAdmin.show', compact('sub_admin','permissions','rights'));
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit($subadmin)
    {
        $id=base64_decode ($subadmin);

        $sub_admin=User ::find($id);

        $role_list  = Roles::NotAdminRole()->get();

        return view('admin.subAdmin.edit', compact('sub_admin','permissions','rights','role_list'));
    }


    public function update(UpdateSubAdminRequest $request, User $sub_admin)
    {
        $exceptFields = [
            '_token',
            '_method',
        ];

        $data = $request->all();
        $updateRecord = [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'full_name' => $data['first_name'] . ' ' . $data['last_name'],
            'user_name' => strtolower($data['first_name'] . $data['last_name']),
            'email' => $data['email'],
            'phone' => phoneFormat($data['phone']),
            'address' => $data['address'] ?? '',
            'role_id' =>$data['role'],

        ];
        if ($request->hasfile('upload_file')) {
            $file = $request->file('upload_file');
            $fileName = $file->getClientOriginalName();
            $file->move(backendUserFile(), $file->getClientOriginalName());
            $updateRecord['profile_picture'] =  url(backendUserFile() .$fileName);

        }



        if ( $request->has('password') && $request->get('password', '') != '' ) {
            $updateRecord['password'] = \Hash::make( $data['password'] );
        }

   $this->userRepository->update($sub_admin->id, $updateRecord);
        return redirect()
            ->route('sub-admin.index')
            ->with('success', 'Sub Admin updated successfully.');
    }

    /**
     * Removes the resource from database.
     */
    public function destroy(User $sub_admin)
    {
        $this->userRepository->delete($sub_admin->id);
        return redirect()
            ->route('sub-admin.index')
            ->with('success', 'Sub Admin  was removed successfully!');
    }

}

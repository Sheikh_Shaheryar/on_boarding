<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreTrainingRequest;
use App\Models\OrderCategory;
use App\Models\Training;
use App\Models\Vendor;
use App\Repositories\Interfaces\TrainingRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TrainingController extends Controller
{
    private $trainingRepository;

    /**
     * Create a new controller instance.
     *
     * @param TrainingRepositoryInterface $trainingRepository
     */


    public function __construct(TrainingRepositoryInterface $trainingRepository)
    {
        $this->middleware('auth:admin');
        parent::__construct();
        $this->trainingRepository = $trainingRepository;
    }


    public function index()
    {
        $data = Training::with('orderCategory', 'vendors');

        return view('admin.training.index', compact('data'));
    }

    public function data(DataTables $datatables, Request $request): JsonResponse
    {
        $query = Training::with('orderCategory', 'vendors')->select('trainings.*');

        return $datatables->eloquent($query)
            ->setRowId(static function ($record) {
                return $record->id;
            })
            ->addColumn('orderCategory', static function ($record) {

                if ($record->orderCategory) {
                    return $record->orderCategory->name;
                }
                return $record = '';
            })
            ->addColumn('vendors', static function ($record) {
                if ($record->vendors) {
                    return $record->vendors->first_name;
                }
                return $record = '';

            })
            ->editColumn('link', static function ($record) {
                return '<a href="' . $record->url . '" title="Preview" class="btn btn-xs btn-primary" target="_blank"> <i class="fa fa-link"></i></a>';
            })
            ->addColumn('action', static function ($record) {
                return backend_view('training.action', compact('record'));
            })
            ->rawColumns(['link', 'orderCategory', 'vendors', 'is_active'])
            ->make(true);
    }

    public function create()
    {
        $data['order_categories'] = OrderCategory::all();
        $data['vendors'] = Vendor::all();
        return view('admin.training.create', $data);
    }

    public function store(StoreTrainingRequest $request)
    {


        $postData = $request->all();

        $order_category_id = $postData['order_category_id'];
        $vendor_id = $postData['vendor_id'];
        if ($postData['type'] == 'orderCategoryDD') {
            $type = 'order_category_id';
            $vendor_id = null;
        } else {
            $order_category_id = null;
            $type = 'vendor_id';
        }

        $file = $request->file('upload_file');
        if ($request->hasfile('upload_file')) {
            $file = $request->file('upload_file');
            $fileName = $file->getClientOriginalName();
            $file->move(backendTrainingFile(), $file->getClientOriginalName());

            $postData['upload_file'] = $fileName;

        }
        $insert = [
            'order_category_id' => $order_category_id,
            'vendors_id' => $vendor_id,
            'name' => $file->getClientOriginalName(),
            'description' => $file->getClientOriginalExtension(),
            'extension' => $file->getClientOriginalExtension(),
            'type' => $file->getClientMimeType(),
            'url' => url(backendTrainingFile() . $file->getClientOriginalName()),
        ];

        $this->trainingRepository->create($insert);
        return redirect()
            ->route('training.index')
            ->with('success', 'Training added successfully.');
    }

    public function destroy(Training $training)
    {
        $data = $training->delete();
        return redirect()
            ->route('training.index')
            ->with('success', 'Training was removed successfully.');
    }


}

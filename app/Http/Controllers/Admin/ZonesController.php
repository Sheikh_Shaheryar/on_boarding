<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreZoneRequest;
use App\Http\Requests\Admin\UpdateZoneRequest;
use App\Models\Zone;
use App\Repositories\Interfaces\ZoneRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ZonesController extends Controller
{
    private $zoneRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct(ZoneRepositoryInterface $zoneRepository)
    {
        $this->middleware('auth:admin');
        parent::__construct();
        $this->zoneRepository = $zoneRepository;
    }


    public function index()
    {
        return view('admin.zones.index');
    }

    public function data(DataTables $datatables, Request $request) : JsonResponse
    {
        $query  = Zone::query();

        return $datatables->eloquent($query)
            ->setRowId(static function ($record) {
                return $record->id;
            })

            ->addColumn('action', static function ($record) {
                return backend_view('zones.action', compact('record') );
            })
            ->rawColumns(['is_active'])
            ->make(true);
    }

    public function create()
    {
        return view('admin.zones.create');
    }


    public function store(StoreZoneRequest $request){
        $data=$request->all();

        $zoneRecord = [
            'name' => $data['name'],
            'latitude' => $data['latitude'],
            'longitude' =>$data['longitude'],
            'radius' => $data['radius'],
            'timezone' =>$data['time_zone'],

            ];
       // dd($zoneRecord);
        $this->zoneRepository->create($zoneRecord);
        return redirect()
            ->route('zones.index')
            ->with('success', 'Zone added successfully.');
    }



    public function edit(Zone $zone){
        return view('admin.zones.edit',compact('zone'));
    }
    public function update (UpdateZoneRequest $updateZoneRequest,Zone $zone){

        $zoneData = $updateZoneRequest->all();

        $updateZoneRecord = [
            'name' => $zoneData['name'],
            'latitude' => $zoneData['latitude'],
            'longitude' =>$zoneData['longitude'],
            'radius' => $zoneData['radius'],
            'timezone' =>$zoneData['time_zone'],
        ];

        $this->zoneRepository->update($zone->id, $updateZoneRecord);
        return redirect()
            ->route('zones.index')
            ->with('success', 'Zone updated successfully.');
    }


    public function destroy(Zone $zone)
    {
        $data = $zone->delete();
        return redirect()
            ->route('zones.index')
            ->with('success', 'Sub Admin was removed successfully.');
    }







}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreVendorsCountRequest;
use App\Http\Requests\Admin\UpdateVendorsCountRequest;
use App\Models\Vendor;
use App\Models\Vendors;
use App\Repositories\Interfaces\VendorsRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class VendorsController extends Controller
{
    private $vendorsRepository;

    /**
     * Create a new controller instance.
     *
     * @param
     */


    public function __construct(VendorsRepositoryInterface $vendorsRepository)
    {
        $this->middleware('auth:admin');
        parent::__construct();
        $this->vendorsRepository = $vendorsRepository;
    }


    public function index()
    {
        return view('admin.vendors.index');
    }

    public function data(DataTables $datatables, Request $request): JsonResponse
    {
        $query = Vendors::with('vendor')->select('vendor_count.*');;
        return $datatables->eloquent($query)
            ->setRowId(static function ($record) {
                return $record->id;
            })
            ->addColumn('vendor', static function ($record) {
                if ($record->vendor) {
                    return $record->vendor->name;
                }
                return $record = '';
            })
            ->addColumn('action', static function ($record) {
                return backend_view('vendors.action', compact('record'));
            })
            ->rawColumns(['vendor', 'is_active'])
            ->make(true);
    }

    public function create()
    {
        $data['vendor'] = Vendor::all();
        return view('admin.vendors.create', $data);
    }


    public function store(StoreVendorsCountRequest $request)
    {
        $data = $request->all();
        $record = [
            'vendor_id' => $data['vendor_id'],
            'order_count' => $data['order_count'],
        ];

        $this->vendorsRepository->create($record);
        return view('admin.vendors.index');
    }

    public function edit(Vendors $vendor)
    {
        $data['vendores'] = Vendor::all();

        return view('admin.vendors.edit', compact('vendor'), $data);
    }

    public function update(UpdateVendorsCountRequest $updateVendorsCountRequest, Vendors $vendor){

        $data = $updateVendorsCountRequest->all();

        $record = [
            'vendor_id' => $data['vendors_id'],
            'order_count' => $data['order_count'],
        ];

        $this->vendorsRepository->update($vendor->id, $record);
        return view('admin.vendors.index');
    }

    public function destroy(Vendors $vendor)
    {
        $data = $vendor->delete();
        return view('admin.vendors.index');
    }


}

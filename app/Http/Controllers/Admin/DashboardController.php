<?php

namespace App\Http\Controllers\Admin;



use App\Models\JobType;
use App\Models\OrderCategory;
use App\Models\QuizAnswer;
use App\Models\QuizQuestion;
use App\Models\Training;
use App\Models\User;
use App\Models\WorkTime;
use App\Models\WorkType;
use App\Models\Zone;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        parent::__construct();
    }

    /**
     * Admin Dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $orderCategory = OrderCategory::all()->count();
        $subAdmin = User::where(['role_id' => User::ROLE_ADMIN])->where(['userType' => 'subadmin'])->get()->count();
        $jobType = JobType::all()->count();
        $zones = Zone::all()->count();
        $worktime = WorkTime::all()->count();
        $worktype = WorkType::all()->count();
        $training = Training::all()->count();
        $quiz = QuizQuestion::all()->count();
        return view('admin.dashboard.index',compact('orderCategory','subAdmin','jobType','zones','worktime','worktype','training','quiz'));

    }
}

<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\Admin\StoreJoeyChecklistRequest;
use App\Http\Requests\Admin\StoreWorkTypeRequest;
use App\Http\Requests\Admin\UpdateJoeyChecklistRequest;
use App\Http\Requests\Admin\UpdateWorkTypeRequest;
use App\Models\JoeyChecklist;
use App\Models\WorkType;
use App\Repositories\Interfaces\JoeyChecklistRepositoryInterface;
use App\Repositories\Interfaces\WorkTypeRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JoeyChecklistController extends Controller
{
    private $joeyChecklistRepository;

    /**
     * Create a new controller instance.
     *
     * @param JoeyChecklistRepositoryInterface $joeyChecklistRepository
     */


    public function __construct(JoeyChecklistRepositoryInterface $joeyChecklistRepository)
    {
        $this->middleware('auth:admin');
        parent::__construct();
        $this->joeyChecklistRepository = $joeyChecklistRepository;
    }


    public function index()
    {

        return view('admin.joeyChecklist.index');
    }

    public function data(DataTables $datatables, Request $request) : JsonResponse
    {
        $query  = JoeyChecklist::query();

        return $datatables->eloquent($query)
            ->setRowId(static function ($record) {
                return $record->id;
            })

            ->addColumn('action', static function ($record) {
                return backend_view('joeyChecklist.action', compact('record') );
            })
            ->rawColumns(['is_active'])
            ->make(true);
    }


    public function create()
    {
        return view('admin.joeyChecklist.create');
    }


    public function store(StoreJoeyChecklistRequest $joeyChecklistRequest){

        $data=$joeyChecklistRequest->all();

        $joeyChecklistRecord = [
            'title' => $data['title'],
        ];
        $this->joeyChecklistRepository->create($joeyChecklistRecord);
        return redirect()
            ->route('joey-checklist.index')
            ->with('success', 'Joey Cheklist added successfully!');
    }


        public function edit(JoeyChecklist $joeyChecklist){
            return view('admin.joeyChecklist.edit',compact('joeyChecklist'));
        }


        public function update (UpdateJoeyChecklistRequest $updateJoeyChecklistRequest,JoeyChecklist $joeyChecklist){

            $data= $updateJoeyChecklistRequest->all();

            $updateJoeyChecklistRecord = [
                'title' => $data['title'],
            ];

            $this->joeyChecklistRepository->update($joeyChecklist->id, $updateJoeyChecklistRecord);
            return redirect()
                ->route('joey-checklist.index')
                ->with('success', 'Joey Checklist is updated successfully!');
        }


          public function destroy(JoeyChecklist $joeyChecklist)          {

              $data = $joeyChecklist->delete();
              return redirect()
                  ->route('joey-checklist.index')
                  ->with('success', 'Joey Checklist removed successfully!');
          }

}

<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateZoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name'        => 'required|string|max:50',
            'latitude'        => 'required|regex:/^[-]?\d+[-]?(\.\d{1,6})?$/',
            'longitude'        =>  'required|regex:/^[-]?\d+[-]?(\.\d{1,6})?$/',
            'radius'        => 'required|regex:/^[-]?\d+[-]?(\.\d{1,6})?$/',
            'time_zone'        => 'required|string|max:100',



        ];
    }
}
